```zsh
$ sudo dnf install clang cmake ninja-build gtk3-devel
$ wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.0.1-stable.tar.xz
$ tar xf flutter_linux_3.0.1-stable.tar.xz
$ mv flutter ~/flutter
$ vim ~/.bashrc
export PATH="$PATH:$HOME/flutter/bin"
$ flutter config --no-analytics
$ flutter precache
$ flutter doctor
```
