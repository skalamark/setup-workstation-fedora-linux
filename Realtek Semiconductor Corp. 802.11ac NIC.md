```zsh
$ git clone https://github.com/brektrou/rtl8821CU.git
$ cd rtl8821CU
$ make clean
$ make
$ sudo make install
$ sudo modprobe 8821cu
$ nmcli radio all; rfkill list all
```
