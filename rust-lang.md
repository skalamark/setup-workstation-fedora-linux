```zsh
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
> 2
> nightly

Current installation options:

   default host triple: x86_64-unknown-linux-gnu
     default toolchain: nightly
               profile: default
  modify PATH variable: yes

$ cargo install exa bat
```
