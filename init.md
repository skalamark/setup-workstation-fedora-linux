# Init

```zsh
$ sudo dnf update

Presumimos que você recebeu as instruções de sempre do administrador
de sistema local. Basicamente, resume-se a estas três coisas:

    #1) Respeite a privacidade dos outros.
    #2) Pense antes de digitar.
    #3) Com grandes poderes vêm grandes responsabilidades.

$ sudo dnf upgrade
$ sudo dnf install dkms make kernel-devel blueman
```
